
from django.contrib.postgres.fields.array import ArrayField
from django.contrib.postgres.fields import JSONField
from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.utils import timezone
from enumfields import EnumField, Enum

from recording.enums import SensorType, HeightType, SizeType, HairType, ConsentType


class Node(models.Model):
    name = models.CharField(max_length=50, default='')
    ip = models.GenericIPAddressField(default='127.0.0.1')
    port = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "{} - {}:{}".format(self.name, self.ip, self.port)


class Sensor(models.Model):
    type = EnumField(SensorType, max_length=1)
    name = models.CharField(max_length=100, default='')

    node = models.ForeignKey('Node', on_delete=models.SET_NULL, related_name="sensors", null=True, blank=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.type)


class PhysicalSetup(models.Model):
    height = models.FloatField(default=2.5)
    name = models.CharField(max_length=50, default='')

    sensors = models.ManyToManyField('Sensor')

    def __str__(self):
        return "{} (Height: {})".format(self.name, self.height)


class Scenario(models.Model):
    id = models.CharField(primary_key=True, max_length=4)
    idx = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=100, default='')
    description = models.TextField(default="")
    particular_needs = models.TextField(blank=True, default='')

    attack = models.BooleanField(default=False)
    nb_people = models.PositiveIntegerField(default=1)

    def __str__(self):
        return "{} - {}".format(self.id, self.title)


class Participant(models.Model):
    height = EnumField(HeightType, max_length=1)
    size = EnumField(SizeType, max_length=1)
    hair = EnumField(HairType, max_length=1)
    wheelchair = models.BooleanField(default=False)
    user_consent = EnumField(ConsentType, max_length=1, default=ConsentType.NO)

    def __str__(self):
        return "Participant n°{}".format(self.pk)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact",)


class Recording(models.Model):
    comment = models.TextField(blank=True, default='')

    scenario = models.ForeignKey('Scenario', on_delete=models.CASCADE, related_name='recordings')
    physical_setup = models.ForeignKey('PhysicalSetup', on_delete=models.CASCADE, default=None)

    participants = models.ManyToManyField('Participant')

    date_creation = models.DateTimeField(auto_now_add=True)

    # idx_list = models.CharField(max_length=10000, validators=[validate_comma_separated_integer_list], default='')
    idx_array = ArrayField(models.IntegerField(), default=list, null=True, blank=True)
    accessories = JSONField(default=dict, null=True, blank=True)

    def __str__(self):
        ids = []
        for id in self.participants.all().values_list('pk', flat=True):
            ids.append(str(id))

        return "Scenario: {}, Participants: {}, Setup: {}, ID: {}".format(self.scenario.id, ','.join(ids), self.physical_setup.name, self.pk)

    # def get_idx_list(self):
    #     return self.idx_list.split(',')