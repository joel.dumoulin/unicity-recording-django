import json
import select

import time

import sys
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from recording.models import Node, Participant, PhysicalSetup, Recording, Scenario, Sensor
from recording.serializers import NodeSerializer, ParticipantSerializer, PhysicalSetupSerializer, RecordingSerializer, ScenarioSerializer, SensorSerializer

import socket

connections = dict()

class NodeViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = Node.objects.all()
    serializer_class = NodeSerializer


class ParticipantViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer


class PhysicalSetupViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = PhysicalSetup.objects.all()
    serializer_class = PhysicalSetupSerializer


class RecordingViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = Recording.objects.all()
    serializer_class = RecordingSerializer

    @detail_route(methods=['get'])
    def prepare(self, request, pk=None):
        recording = self.get_object()
        print("PREPARE")

        ok = list()
        nok = list()

        # Close existing connection and clear dict
        for key, connection in connections.items():
            connection.close()
        connections.clear()

        # Wait a bit
        time.sleep(0.5)

        for sensor in recording.physical_setup.sensors.all():
            sensor_id = sensor.id
            ip = sensor.node.ip
            port = sensor.node.port

            try:
                # Create socket for each node
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Open socket for each node
                s.connect((ip, port))

                connections[sensor_id] = s
                ok.append(sensor_id)
            except:
                print("Error while creating socket for : {}:{}".format(ip, port))
                nok.append(sensor_id)

        return Response({"ok": ok, "nok": nok})

    @detail_route(methods=['post'])
    def start(self, request, pk=None):
        recording = self.get_object()

        ok = list()
        nok = list()

        print("START")

        scenario_id = recording.scenario.id

        # Change idx...
        idx = recording.scenario.idx
        idx = idx + 1
        recording.scenario.idx = idx
        recording.idx_array.append(idx)
        str_idx = "%03d" % (idx, )

        # Add accessories
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        accessories = body['accessories']

        if(len(accessories) > 0):
            recording.accessories[idx] = accessories

        # startRecording for each node
        for sensor in recording.physical_setup.sensors.all():
            sensor_name = sensor.name
            ip = sensor.node.ip
            port = sensor.node.port

            msg = 'startRecording;{};{};{}'.format(sensor_name, scenario_id, str_idx).encode()

            try:
                s = connections[sensor.id]

                try:
                    ready_to_read, ready_to_write, in_error = select.select([s,], [s,], [])
                except select.error:
                    print("Error while starting sensor : {} ({}:{})".format(sensor_name, ip, port))
                    nok.append(sensor.id)

                if len(ready_to_write) > 0:
                    s.send(msg)
                    print("{}".format(msg))
            except:
                print("Error while starting sensor : {} ({}:{})".format(sensor_name, ip, port))
                nok.append(sensor.id)

        for sensor in recording.physical_setup.sensors.all():
            try:
                s = connections[sensor.id]
                recv = s.recv(4096)

                if recv:
                    ok.append(sensor.id)
                else:
                    nok.append(sensor.id)
            except:
                nok.append(sensor.id)

        recording.scenario.save()
        recording.save()

        return Response({"ok": ok, "nok": nok})

    @detail_route(methods=['get'])
    def stop(self, request, pk=None):
        recording = self.get_object()

        ok = list()
        nok = list()
        nb_frames = dict()

        print("STOPPING")

        # stopRecording for each node
        msg = 'stopRecording;'.encode()
        for key, connection in connections.items():
            try:
                ready_to_read, ready_to_write, in_error = select.select([connection, ], [connection, ], [])
            except select.error:
                print("Error while stopping sensor...")
                nok.append(key)

            if len(ready_to_write) > 0:
                connection.send(msg)

        for sensor in recording.physical_setup.sensors.all():
            try:
                s = connections[sensor.id]
                recv = s.recv(4096)

                if recv:
                    if recv == "-1":
                        nok.append(sensor.id)
                    else:
                        ok.append(sensor.id)

                    nb_frames[sensor.name] = recv
                else:
                    nok.append(sensor.id)
            except:
                nok.append(sensor.id)

        min_nb_frames = sys.maxsize
        max_nb_frames = -1
        for key, val in nb_frames.items():
            min_nb_frames = min(min_nb_frames, int(val))
            max_nb_frames = max(max_nb_frames, int(val))

        drift = max_nb_frames-min_nb_frames

        print("Drift: {} ({} - {})".format(drift, max_nb_frames, min_nb_frames))

        return Response({"ok": ok, "nok": nok, "nbFrames": nb_frames, "drift": drift})

    @list_route(methods=['get'])
    def quit(self, request):
        print("QUIT!")

        ok = list()
        nok = list()

        # stop for each node
        msg = 'stop;'.encode()
        for key, connection in connections.items():
            try:
                connection.send(msg)
                connection.close()
                ok.append(key)
            except:
                print("Error while quitting sensor")
                nok.append(key)

        connections.clear()

        return Response({"ok": ok, "nok": nok})

    @list_route(methods=['get'])
    def filter(self, request):
        query_params = request.query_params
        str_accessories = query_params.get('accessories', None)

        accessories = []

        if str_accessories is not None:
            for accessory in str_accessories.split('|'):
                accessories.append(accessory)

        print("Query params")
        print(accessories)

        res = dict()

        for recording in Recording.objects.all():
            for idx, accessories_list in recording.accessories.items():
                for item in accessories:
                    if item in accessories_list:
                        if recording.id not in res:
                            res[recording.id] = set()

                        res[recording.id].add(idx)

        return Response(res)


class ScenarioViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = Scenario.objects.all()
    serializer_class = ScenarioSerializer


class SensorViewSet(viewsets.ReadOnlyModelViewSet):
    """

    """
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer