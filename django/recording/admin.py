from django import forms
from django.contrib import admin

# Register your models here.
from recording.models import Sensor, PhysicalSetup, Node, Participant, Recording, Scenario

class NodeForm(forms.ModelForm):
    class Meta:
        model = Node
        exclude = []

    sensors = forms.ModelMultipleChoiceField(queryset=Sensor.objects.all())

    def __init__(self, *args, **kwargs):
        super(NodeForm, self).__init__(*args, **kwargs)
        if self.instance:
            if self.instance.sensors:
                self.fields['sensors'].initial = self.instance.sensors.all()
            else:
                self.fields['sensors'].initial = []

    def save(self, *args, **kwargs):
        instance = super(NodeForm, self).save(commit=False)
        instance.save()
        self.fields['sensors'].initial.update(node=None)
        self.cleaned_data['sensors'].update(node=instance)
        return instance


class NodeAdmin(admin.ModelAdmin):
    form = NodeForm

admin.site.register(Node, NodeAdmin)

admin.site.register(Sensor)

admin.site.register(PhysicalSetup)


class ScenarioAdmin(admin.ModelAdmin):
    fields = ['id', 'idx', 'title', 'description', 'particular_needs', 'attack', 'nb_people']
    readonly_fields = ['idx']

admin.site.register(Scenario, ScenarioAdmin)

admin.site.register(Participant)


class RecordingAdmin(admin.ModelAdmin):
    fields = ['id', 'comment', 'scenario', 'physical_setup', 'participants', 'idx_array', 'accessories']
    readonly_fields = ['id', 'idx_array', 'accessories']

    # define the raw_id_fields
    raw_id_fields = ('participants',)
    # define the autocomplete_lookup_fields
    autocomplete_lookup_fields = {
        'm2m': ['participants'],
    }

admin.site.register(Recording, RecordingAdmin)

