# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-28 16:03
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recording', '0019_auto_20170928_1747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recording',
            name='idx_array',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), default=list, size=None),
        ),
    ]
