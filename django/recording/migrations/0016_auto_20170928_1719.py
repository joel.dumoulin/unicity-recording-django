# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-28 15:19
from __future__ import unicode_literals

import django.contrib.postgres.fields
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recording', '0015_auto_20170919_1546'),
    ]

    operations = [
        migrations.AddField(
            model_name='recording',
            name='accessories',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='recording',
            name='idx_array',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), blank=True, null=True, size=None),
        ),
    ]
