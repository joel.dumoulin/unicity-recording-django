from enumfields import Enum

class SensorType(Enum):
    KINECT = 'k'
    ARGOS = 'a'
    FOTONIC = 'f'

    class Labels:
        KINECT = 'Kinect'
        ARGOS = 'Argos'
        FOTONIC = 'Fotonic'


class HeightType(Enum):
    NORMAL = 'n'
    SMALL = 's'
    TALL = 't'

    class Labels:
        NORMAL = 'Normal'
        SMALL = 'Small'
        TALL = 'Tall'


class SizeType(Enum):
    NORMAL = 'n'
    THIN = 't'
    FAT = 'f'

    class Labels:
        NORMAL = 'Normal'
        THIN = 'Thin'
        FAT = 'Fat'


class HairType(Enum):
    DARK = 'd'
    FAIR = 'f'
    NO = 'n'

    class Labels:
        DARK = 'Dark'
        FAIR = 'Fair'
        NO = 'No hair'


class ConsentType(Enum):
    NO = 'n'
    O3D = '3'
    VIDEO = 'v'
    FULL = 'f'

    class Labels:
        NO = 'NO'
        O3D = '3D only'
        VIDEO = 'Video and 3D'
        FULL = 'Filming and sharing'