from rest_framework import serializers
from recording.models import Node, Participant, PhysicalSetup, Recording, Scenario, Sensor
from drf_enum_field.serializers import EnumFieldSerializerMixin

class ParticipantSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = ('id', 'height', 'size', 'hair', 'wheelchair', 'user_consent')


class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        fields = ('name', 'ip', 'port')


class SensorSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    node = NodeSerializer(read_only=True)

    class Meta:
        model = Sensor
        fields = ('id', 'type', 'name', 'node')


class PhysicalSetupSerializer(serializers.ModelSerializer):
    sensors = SensorSerializer(many=True, read_only=True)

    class Meta:
        model = PhysicalSetup
        fields = ('height', 'name', 'sensors')


class ScenarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scenario
        fields = ('id', 'idx', 'title', 'description', 'particular_needs', 'attack', 'nb_people')


class RecordingSerializer(serializers.ModelSerializer):
    physical_setup = PhysicalSetupSerializer(read_only=True)
    participants = ParticipantSerializer(many=True, read_only=True)
    scenario = ScenarioSerializer(read_only=True)

    class Meta:
        model = Recording
        fields = ('id', 'comment', 'scenario', 'physical_setup', 'participants', 'idx_array', 'accessories')