"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'unicity-recording-django.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        
        # # append an app list module for "Administration"
        # self.children.append(modules.ModelList(
        #     _('ModelList: Administration'),
        #     column=1,
        #     collapsible=False,
        #     models=('django.contrib.*',),
        # ))

        self.children.append(modules.Group(
            _('Preparation'),
            column=1,
            collapsible=True,
            children = [
                modules.ModelList(
                    title='1. Prepare sensors configuration',
                    column=1,
                    models=('recording.models.Sensor', 'recording.models.Node', 'recording.models.PhysicalSetup',)
                ),
                modules.ModelList(
                    title='2. Prepare scenarios',
                    column=1,
                    models=('recording.models.Scenario',)
                )
            ]
        ))

        self.children.append(modules.Group(
            _('Record'),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    title='3. Add participants',
                    column=1,
                    models=('recording.models.Participant',)
                ),
                modules.ModelList(
                    title='4. Create recording',
                    column=1,
                    models=('recording.models.Recording',)
                )
            ]
        ))

        # # append a recent actions module
        # self.children.append(modules.RecentActions(
        #     _('Recent Actions'),
        #     limit=5,
        #     collapsible=False,
        #     column=3,
        # ))


