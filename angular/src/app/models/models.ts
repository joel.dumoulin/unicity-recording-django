export class Recording {
  constructor(
    public id: number,
    public comment: string,
    public scenario: Scenario,
    public physical_setup: PhysicalSetup,
    public participants: Participant[],
    public idx_list: Array<string>,
    public accessories: {}
  ) {}
}

export class Scenario {
  constructor(
    public id: string,
    public idx: number,
    public title: string,
    public description: string,
    public particular_needs: string,
    public attack: boolean,
    public nb_people: number
  ){}
}

export class PhysicalSetup {
  constructor(
    public height: number,
    public name: string,
    public sensors: Sensor[]
  ){}
}

export class Sensor {
  constructor(
    public type: string,
    public name: string,
    public node: Node
  ){}
}

export class Node {
  constructor(
    public name: string,
    public ip: string,
    public port: number
  ){}
}

export class Participant {
  constructor(
    public id: number,
    public height: string,
    public size: string,
    public hair: string,
    public wheelchair: boolean
  ){}
}
