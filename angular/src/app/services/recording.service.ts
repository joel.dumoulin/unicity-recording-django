import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Recording } from '../models/models';

@Injectable()
export class RecordingService {
  API_URL : String = "http://localhost:8000/api";

  constructor(private http: Http) { }

  getRecordings(): Observable<Recording[]>{
    return this.http.get(`${this.API_URL}/recordings/`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateRecording(id: number): Observable<Recording>{
    return this.http.get(`${this.API_URL}/recordings/${id}/`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  start(id: number, accessories: Array<string>): Observable<any>{
    let data = {
      'accessories': accessories
    }
    return this.http.post(`${this.API_URL}/recordings/${id}/start/`, data)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  stop(id: number): Observable<any>{
    return this.http.get(`${this.API_URL}/recordings/${id}/stop/`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  prepare(id: number): Observable<any>{
    return this.http.get(`${this.API_URL}/recordings/${id}/prepare/`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  quit(): Observable<any>{
    return this.http.get(`${this.API_URL}/recordings/quit/`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
