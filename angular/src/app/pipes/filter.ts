import { Pipe, PipeTransform } from '@angular/core';

import { Recording } from '../models/models';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(recordings: Recording[], recordingId: string, participantId: string): any {
    let _recordingId : number = parseInt(recordingId);

    //check if search term is undefined
    if(_recordingId >= 0)
      recordings = recordings.filter(function(recording){
          return recording.id == _recordingId;
      });


    if(participantId){
      let _strParticipantIds: Array<string> = participantId.split(" ");
      let _participantIds: Array<number> = [];

      for(let _strId of _strParticipantIds){
        let id: number = parseInt(_strId);
        if(id >= 0){
          _participantIds.push(id);
        }
      }


      if(_participantIds.length > 0)
        recordings = recordings.filter(function(recording){
            let hasParticipant = false;
            for(let participant of recording.participants){
              if(_participantIds.includes(participant.id)) return true;
            }
            return false;
        });
    }

    return recordings;
  }
}
