import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatInputModule, MatListModule, MatMenuModule, MatToolbarModule, MatTooltipModule, MatIconModule } from '@angular/material';

import { ToasterModule } from 'angular2-toaster';

import { FlexLayoutModule } from '@angular/flex-layout';

import { FilterPipe } from './pipes/filter';

import { RecordingService } from './services/recording.service';
import { ScenarioComponent } from './components/scenario/scenario.component';
import { SetupComponent } from './components/setup/setup.component';
import { ParticipantsComponent } from './components/participants/participants.component';
import { RecorderComponent } from './components/recorder/recorder.component';

@NgModule({
  declarations: [
    AppComponent,
    ScenarioComponent,
    SetupComponent,
    ParticipantsComponent,
    FilterPipe,
    RecorderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    FlexLayoutModule,
    ToasterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatCardModule,
    MatChipsModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatTooltipModule,
    MatIconModule
  ],
  providers: [RecordingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
