import { Component } from '@angular/core';

import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import { RecordingService } from './services/recording.service';
import { Recording } from './models/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Recording manager';

  recordings: Recording[] = [];

  selectedRecording: Recording;

  filterRecordingId: string;
  filterParticipantId: string;

  isRecording: boolean;

  ok: Array<number>;
  nok: Array<number>;
  nbFrames: {};
  drift: number;

  accessories = {};

  constructor(private recordingService: RecordingService, private toasterService: ToasterService){}

  ngOnInit() {
    this.getRecordings();
  }

  getRecordings(){
    this.recordingService.getRecordings()
      .subscribe(recordings => {
        this.recordings = recordings;
      })
  }

  changeRecording(recording: Recording){
    if(!this.isRecording){
      this.recordingService.updateRecording(recording.id)
        .subscribe(res => {
          this.selectedRecording = res;
          this.updateAccessories();

          this.recordingService.prepare(recording.id)
            .subscribe(res => {
              this.ok = res.ok;
              this.nok = res.nok;

              this.updateSensorsStatus(true,
                "All sensors are connected!",
                "One or more sensors have not been connected successfully. Try reloading the recording before continuing."
              );
            });
        });
    }
  }

  updateRecording(){
    this.recordingService.updateRecording(this.selectedRecording.id)
      .subscribe(res => {
        this.selectedRecording = res;
        this.updateAccessories();
      });
  }

  quit(){
    this.selectedRecording = null;

    this.recordingService.quit()
      .subscribe(res => {
        this.ok = res.ok;
        this.nok = res.nok;

        this.updateSensorsStatus(false,
          "All sensors have been disconnected!",
          "One or more sensors have not been disconnected successfully."
        );
      });
  }

  onRecordingEvent(recording: boolean){
    this.isRecording = recording;
    this.updateRecording();

    if(this.isRecording){
      this.toasterService.clear();
    }
  }

  onOkNokEvent(res: any) {
    this.ok = res.ok;
    this.nok = res.nok;

    let okMsg = this.isRecording ? "Started recording" : "Recording stopped";
    let nokMsg = this.isRecording ? "Start" : "Stop";
    this.updateSensorsStatus(false,
      okMsg,
      nokMsg + " has not been correctly sent to one or more sensors. Try reloading the recording before continuing."
    );

    if(!this.isRecording && res.nbFrames){
      this.nbFrames = res.nbFrames;
      if(Object.keys(this.nbFrames).length){
        this.drift = res.drift;
        this.showNbFrames();
      }
    }
  }

  updateSensorsStatus(showAlert: boolean, okMsg: string, nokMsg: string){
    let toast: Toast;

    if(this.nok.length > 0){
      if(showAlert){
        alert(nokMsg);
        return;
      }else{
        toast = {
          type: 'error',
          title: 'Connection error',
          body: nokMsg,
          showCloseButton: true,
          timeout: -1
        };
      }
    }else{
      toast = {
        type: 'success',
        title: okMsg,
        timeout: 2000
      };
    }

    this.toasterService.pop(toast);
  }

  showNbFrames(){
    let toast: Toast;

    let msg: string = "<br/>";

    let _type: string = "success";

    for(let key in this.nbFrames){
      let value = this.nbFrames[key];
      if(value == "-1" || value == "0"){
        _type = "error";
      }

      msg = msg + "<b>" + key + ":</b> " + value + " f</br>";
    }

    msg = msg + "<hr>" + "Drift: " + this.drift + " f";

    toast = {
      type: _type,
      title: 'Recording report',
      body: msg,
      showCloseButton: true,
      timeout: -1,
      bodyOutputType: BodyOutputType.TrustedHtml
    };

    this.toasterService.pop(toast);
  }

  updateAccessories(){
    this.accessories = {};
    for(let key in this.selectedRecording.accessories){
      let value: Array<string> = this.selectedRecording.accessories[key];
      let str_accessories: string = value.join(", ");
      this.accessories[key] = str_accessories;
    }
  }

  getAccessoriesList(idx: number){
    if(this.accessories.hasOwnProperty(idx)){
      return this.accessories[idx];
    }else{
      return "";
    }
  }

  hasAccessories(idx: number): boolean{
    if(this.accessories.hasOwnProperty(idx)){
      return true;
    }else{
      return false;
    }
  }
}
