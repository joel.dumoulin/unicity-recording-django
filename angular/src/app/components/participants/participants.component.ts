import { Component, OnInit, Input } from '@angular/core';

import { Participant } from '../../models/models';

@Component({
  selector: 'participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.css']
})
export class ParticipantsComponent implements OnInit {

  @Input() participants: Participant[];

  constructor() { }

  ngOnInit() {
  }

}
