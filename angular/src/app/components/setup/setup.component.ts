import { Component, OnInit, Input } from '@angular/core';

import { PhysicalSetup } from '../../models/models';

@Component({
  selector: 'setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {

  @Input() physicalSetup: PhysicalSetup;

  @Input() ok: Array<number> = new Array();
  @Input() nok: Array<number> = new Array();

  constructor() { }

  ngOnInit() {
  }

  isConnected(id: number): boolean {
    return id && this.ok && this.ok.includes(id);
  }

  isDisconnected(id: number): boolean {
    return id && this.nok && this.nok.includes(id);
  }

}
