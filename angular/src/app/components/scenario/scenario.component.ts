import { Component, OnInit, Input } from '@angular/core';

import { Scenario } from '../../models/models'

@Component({
  selector: 'scenario',
  templateUrl: './scenario.component.html',
  styleUrls: ['./scenario.component.css']
})
export class ScenarioComponent implements OnInit {
  @Input() scenario: Scenario;

  constructor() { }

  ngOnInit() {
  }

}
