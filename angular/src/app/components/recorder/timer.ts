export class Timer {

  private _minutes: number = 0;
  private _secondes: number = 0;
  private _totalSecondes: number = 0;
  private _timer;

  get minutes(): string { return ("0" +  this._minutes).slice(-2); }
  get secondes(): string { return ("0" +  this._secondes).slice(-2); }

  start() {
    this._timer = setInterval(() => {
      this._minutes = Math.floor(++this._totalSecondes / 60);
      this._secondes = this._totalSecondes - this._minutes * 60;
    }, 1000);
  }

  stop() {
    clearInterval(this._timer);
  }

  reset() {
    this._totalSecondes = this._minutes = this._secondes = 0;
  }
}
