export class State {

  private _playing: boolean = false;
  private _stopped: boolean = true;

  get stopped(): boolean { return this._stopped; }
  get playing(): boolean { return this._playing; }

  setPlay() {
    this._stopped = false;
    this._playing = true;
  }

  setStop() {
    this._stopped = true;
    this._playing = false;
  }
}
