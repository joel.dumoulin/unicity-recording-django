import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Timer } from './timer';
import { State } from './state';

import { RecordingService } from '../../services/recording.service';

@Component({
  selector: 'recorder',
  templateUrl: './recorder.component.html',
  styleUrls: ['./recorder.component.css']
})
export class RecorderComponent implements OnInit {
  _timer: Timer = new Timer();
  _state: State = new State();

  @Input() recordingId: number;
  @Output() onRecording = new EventEmitter<boolean>();
  @Output() onOkNok = new EventEmitter<any>();

  // Accessories
  backpack: boolean = false;
  hood: boolean = false;
  coat: boolean = false;
  blanket: boolean = false;
  umbrella: boolean = false;
  other: boolean = false;

  constructor(private recordingService: RecordingService) { }

  ngOnInit() {
  }

  start() {
    this.onRecording.emit(true);

    let accessories: Array<string> = [];

    if(this.backpack){
      accessories.push("backpack");
    }
    if(this.hood){
      accessories.push("hood");
    }
    if(this.coat){
      accessories.push("coat");
    }
    if(this.blanket){
      accessories.push("blanket");
    }
    if(this.umbrella){
      accessories.push("umbrella");
    }
    if(this.other){
      accessories.push("other");
    }

    this.recordingService.start(this.recordingId, accessories)
      .subscribe(res => {
        this.onOkNok.emit(res);
      });

    this._timer.reset();
    this._timer.start();
    this._state.setPlay();
  }

  stop() {
    this.onRecording.emit(false);

    this.recordingService.stop(this.recordingId)
      .subscribe(res => {
        this.onOkNok.emit(res);
      });

    this._timer.stop();
    this._state.setStop();
  }

  reset() {
    this._timer.reset();
  }
}
