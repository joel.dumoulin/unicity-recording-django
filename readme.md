
# Migrate & create superuser

```
docker-compose run django python manage.py makemigrations
docker-compose run django python manage.py migrate
docker-compose run django python manage.py createsuperuser
```

The superuser username and password is up to you

# Load data

First copy the .json file alongside the manage.py file, then execute the following command using the correct version:

```
docker-compose run django python manage.py loaddata backup_${version}.json
```

# Launch services

```
docker-compose up
```

# Use services
## API
Go to http://localhost:8000/api

## Recording interface
Go to http://localhost:4200
